package com.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.model.Tienda;
import com.demo.repo.TiendaDao;

@Service
public class TiendaService {

	@Autowired
	private TiendaDao tiendaDao;
	public Tienda agregarTienda(Tienda tienda) {
		return tiendaDao.save(tienda);
	}
}
