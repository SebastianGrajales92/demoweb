package com.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.model.Autor;
import com.demo.repo.AutorDao;

@Service
public class AutorService {
	
	@Autowired
	private AutorDao autorDao;
	
	public Autor agregarAutor(Autor autor) {
		return autorDao.save(autor);
	}

}
