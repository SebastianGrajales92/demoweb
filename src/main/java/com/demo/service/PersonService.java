package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.model.Person;
import com.demo.repo.IPersonaDao;

@Service
public class PersonService {

	@Autowired
	private IPersonaDao personDao;
	
	public List<Person> obtenerPersonas() {
		return personDao.findAll();
	}
	
	public Person obtenerPersona(String nombre) {
		return personDao.findByNombre(nombre);
	}
}
