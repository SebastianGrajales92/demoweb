package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.demo.model.Producto;
import com.demo.repo.ProductoDao;

@Service
public class ProductoService {

	@Autowired
	private ProductoDao productoDao;
	
	public Producto agregarProducto(Producto producto) {
		return productoDao.save(producto);
	}
	
	
	public List<Producto> obtenerProductos(){
		return productoDao.findAll();
	}
}
