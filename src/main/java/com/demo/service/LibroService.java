package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.model.Autor;
import com.demo.model.Libro;
import com.demo.repo.LibroDao;
import com.demo.response.RespuestaPersonalizada;

@Service
public class LibroService {

	@Autowired
	public LibroDao libroDao;
	public Libro agregarLibro(Libro libro) {
		return libroDao.save(libro);
	}
	
	public ResponseEntity<Object> obtenerLibros(){
		ResponseEntity<Object> respuesta;
		try {
			List<Libro> libros = libroDao.findAll();
			respuesta = ResponseEntity.ok(HttpStatus.OK);
			respuesta = new ResponseEntity<>(libros,HttpStatus.OK);
		} catch (Exception e) {
			respuesta = ResponseEntity.ok(HttpStatus.BAD_REQUEST);
			respuesta = new ResponseEntity<>("Disculpenos tenemos un error",HttpStatus.BAD_REQUEST);
		}
		return respuesta;
	}
	
	public List<Libro> obtenerLibrosAutor(Autor autor){
		return libroDao.findByAutor(autor);
	}
	
	public ResponseEntity<Object> obtenerLibrosIdAutor(Integer idAutor){
		ResponseEntity<Object> respuesta;
		try {
			List<Libro> libros = libroDao.findByIdAutor(idAutor);
			if(libros.size()==0) {
				throw new Exception("Error");
			}
			RespuestaPersonalizada res = new RespuestaPersonalizada("Busqueda de libros con exito", HttpStatus.OK);
			res.setObjectoRespuesta(libros);
			respuesta = ResponseEntity.ok(HttpStatus.OK);
			respuesta = new ResponseEntity<>(res,HttpStatus.OK);
		} catch (Exception e) {
			respuesta = ResponseEntity.ok(HttpStatus.BAD_REQUEST);
			RespuestaPersonalizada res = new RespuestaPersonalizada("Error buscando libros", HttpStatus.BAD_REQUEST);
			respuesta = new ResponseEntity<>(res,HttpStatus.BAD_REQUEST);
		}
		return respuesta;
	}
	
	public void actualizarAutorLibro(Integer idLibro, Integer idAutor) {
		libroDao.actualizarAutor(idLibro, idAutor);
	}
	
	public List<Libro> obtenerLibrosMayores40(){
		return libroDao.obtenerLibrosEscritosMayores40();
	}
}
