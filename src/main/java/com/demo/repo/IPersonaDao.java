package com.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.model.Person;

public interface IPersonaDao extends JpaRepository<Person, Integer>{

	
	public Person findByNombre(String nombre);
	
	public Person findByApellido(String apellido);
}
