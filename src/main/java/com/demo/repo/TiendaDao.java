package com.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.model.Tienda;

public interface TiendaDao extends JpaRepository<Tienda, Integer>{

}
