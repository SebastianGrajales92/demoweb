package com.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.model.Autor;

@Repository
public interface AutorDao extends JpaRepository<Autor, Integer>{

}
