package com.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.model.Producto;

public interface ProductoDao extends JpaRepository<Producto, Integer>{

	
}
