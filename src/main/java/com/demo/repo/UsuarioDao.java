package com.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.model.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Integer>{

	public Usuario findByNombre(String nombre);
}
