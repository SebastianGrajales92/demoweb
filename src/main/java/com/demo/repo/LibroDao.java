package com.demo.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.model.Autor;
import com.demo.model.Libro;

@Repository
@Transactional
public interface LibroDao extends JpaRepository<Libro, Integer>{

	public List<Libro> findByAutor(Autor autor);
	
	@Query(value = "select * from libros where autor_id=?", nativeQuery = true)
	public List<Libro> findByIdAutor(Integer idAutor);
	
	@Modifying
	@Query(value = "UPDATE libros SET autor_id = ?2 WHERE (id = ?1)", nativeQuery = true)
	public void actualizarAutor(Integer idLibro, Integer idAutor);
	
	@Query(value = "select * from libros l join autores a on l.autor_id = a.id where a.edad >= 40", nativeQuery = true)
	public List<Libro> obtenerLibrosEscritosMayores40();
}
