package com.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Tienda;
import com.demo.service.TiendaService;

@RestController
@RequestMapping("api/tienda")
public class TiendaController {

	@Autowired
	private TiendaService tiendaService;
	@PostMapping(value = "agregar-tienda")
	public Tienda agregarTienda(@RequestBody Tienda tienda) {
		return tiendaService.agregarTienda(tienda);
	}
}
