package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Person;
import com.demo.repo.IPersonaDao;
import com.demo.service.PersonService;


@RestController
@RequestMapping("api/demo")
public class DemoController {

	@Autowired
	private PersonService personaService;
	
		@GetMapping("listar-personas")
		public List<Person> personas() {
			return personaService.obtenerPersonas();
		}
		
		@GetMapping("listar-persona")
		public Person obtenerPersona(@RequestParam String nombre) {
			return personaService.obtenerPersona(nombre);
		}
		
		/*
		@PostMapping("agregar-persona")
		public Person adicionarPersona(@RequestBody Person personaNueva) {
			return personDao.save(personaNueva);
		}
		
		@PostMapping("agregar-personas")
		public List<Person> adicionarPersonas(@RequestBody List<Person> personasNuevas){
			return personDao.saveAll(personasNuevas);
		}
		
		@PutMapping("modificar-persona")
		public Person modificarPersona(@RequestBody Person personaNueva) {
			return personDao.findById(personaNueva.getId())
					.map(persona->{
						persona.setNombre(personaNueva.getNombre());
						return personDao.save(persona);
					}).orElseGet(() ->{
						return personDao.save(personaNueva);
					});
		}
		
		@PutMapping("modificar-persona-simple")
		public void modificarPersonaSimple(@RequestBody Person personaNueva) {
			personDao.save(personaNueva);
		}
		
		@PutMapping("modificar-persona-parametros")
		public void modificarPersonaParametro(@RequestParam Integer id, @RequestParam String apellido) {
			Person persona = personDao.findById(id).orElse(null);
			persona.setApellido(apellido);
			personDao.save(persona);
		}
		
		@PutMapping("modificar-persona-body")
		public void modificarPersonaBody(@RequestBody Person personaActualizar) {
			Person persona = personDao.findByApellido(personaActualizar.getApellido());
			persona.setEdad(personaActualizar.getEdad());
			personDao.save(persona);
		}
		
		@DeleteMapping("eliminar-persona-id")
		public void eliminarPersona(@RequestParam Integer id) {
			personDao.deleteById(id);
		}
		
		@DeleteMapping("eliminar-todos")
		public void eliminarTodos() {
			personDao.deleteAll();
		}*/
}
