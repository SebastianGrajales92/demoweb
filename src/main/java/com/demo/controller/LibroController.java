package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Autor;
import com.demo.model.Libro;
import com.demo.service.LibroService;

@RestController
@RequestMapping("api/demo/libros/")
public class LibroController {

	@Autowired
	private LibroService libroService;
	
	@PostMapping(value = "post")
	public Libro agregarLibro(@RequestBody Libro libro) {
		return libroService.agregarLibro(libro);
	}
	
	@GetMapping(value = "get")
	public ResponseEntity<Object> obtenerLibros(){
		return libroService.obtenerLibros();
	}
	
	@GetMapping(value = "get-libro-autor")
	public List<Libro> obtenerLibroAutor(@RequestBody Autor autor){
		return libroService.obtenerLibrosAutor(autor);
	}
	
	@GetMapping(value = "get-libro-id")
	public ResponseEntity<Object> obtenerLibroIdAutor(@RequestParam Integer idAutor){
		return libroService.obtenerLibrosIdAutor(idAutor);
	}
	
	@PutMapping(value = "put-libro")
	public void actualizarAutorLibro(@RequestParam Integer idLibro, @RequestParam Integer idAutor) {
		libroService.actualizarAutorLibro(idLibro, idAutor);
	}
	
	@GetMapping(value = "get-40")
	public List<Libro> obtenerLibrosMayores40(){
		return libroService.obtenerLibrosMayores40();
	}
}
