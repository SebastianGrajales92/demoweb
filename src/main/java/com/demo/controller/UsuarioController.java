package com.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Usuario;
import com.demo.service.UsuarioService;

@RestController
@RequestMapping("api/user/")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping(value = "crear")
	public ResponseEntity<Object> crearUsuario(@RequestBody Usuario usuario){
		return usuarioService.agregarUsuario(usuario);
	}
	
	
}
