package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Producto;
import com.demo.service.ProductoService;

@RestController
@RequestMapping("api/producto")
public class ProductoController {

	@Autowired
	ProductoService productoService;
	
	@PostMapping(value = "agregar-prod")
	public Producto agregarProducto(@RequestBody Producto producto) {
		return productoService.agregarProducto(producto);
	}
	
	@GetMapping(value = "get-all")
	public List<Producto> obtenerProductos(){
		return productoService.obtenerProductos();
	}
}
