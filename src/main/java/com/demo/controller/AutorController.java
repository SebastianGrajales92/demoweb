package com.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Autor;
import com.demo.service.AutorService;

@RestController
@RequestMapping("api/demo/")
public class AutorController {

	@Autowired
	private AutorService autorService;
	
	@PostMapping(value = "post")
	public Autor agregarAutor(@RequestBody Autor autor) {
		return autorService.agregarAutor(autor);
	}
}
