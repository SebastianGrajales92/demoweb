package com.demo;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.coyote.http11.Http11AprProtocol;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.event.annotation.BeforeTestExecution;
import org.springframework.test.context.event.annotation.BeforeTestMethod;

import com.demo.model.Autor;
import com.demo.model.Libro;
import com.demo.repo.LibroDao;
import com.demo.service.LibroService;

public class LibroServiceTest {
	
	private LibroDao libroDao = mock(LibroDao.class);
	
	@Autowired
	private LibroService libroService = new LibroService();
	
	
	
	@Test
	public void testObtenerLibrosCatch() {
		cambiarValueDao();
		ResponseEntity<Object> respuesta = libroService.obtenerLibros();
		assertEquals(HttpStatus.BAD_REQUEST, respuesta.getStatusCode());
	}


	private void cambiarValueDao() {
		libroService.libroDao = null;
	}
	
	
	
	void setup() {
		Autor autor = new Autor();
		autor.setNombre("Pepe");
		autor.setApellidos("garcia");
		autor.setAñosExperiencia(3);
		autor.setEdad(40);
		autor.setId(1);
		Libro libroMock = new Libro();
		libroMock.setId(1);
		libroMock.setTitulo("Titulo");
		libroMock.setPrecio(25000);
		libroMock.setTema("astro");
		libroMock.setAutor(autor);
		List<Libro> librosMock = new ArrayList<>();
		librosMock.add(libroMock);
		when(libroDao.findAll()).thenReturn(librosMock);
	}
	
	@Test
	public void obtenerLibros(){
		setup();
		libroService.libroDao = libroDao;
		ResponseEntity<Object> respuesta = libroService.obtenerLibros();
		/* List<Libro> libros= new ArrayList<Libro>();
		Autor autor = new Autor();
		autor.setNombre("Juan");
		autor.setApellidos("Orozco");
		autor.setAñosExperiencia(3);
		autor.setEdad(40);
		autor.setId(1);
		Libro libroMock = new Libro();
		libroMock.setId(1);
		libroMock.setTitulo("Titulo");
		libroMock.setPrecio(25000);
		libroMock.setTema("astro");
		libroMock.setAutor(autor);
		libros.add(libroMock);
		assertNotNull(respuesta);*/
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
	}
	
	
	
	void setup2() {
		Autor autor = new Autor();
		autor.setNombre("Pepe");
		autor.setApellidos("garcia");
		autor.setAñosExperiencia(3);
		autor.setEdad(40);
		autor.setId(1);
		Libro libroMock = new Libro();
		libroMock.setId(1);
		libroMock.setTitulo("Titulo");
		libroMock.setPrecio(25000);
		libroMock.setTema("astro");
		libroMock.setAutor(autor);
		Optional<Libro> libroMockOptional = Optional.of(Optional.of(libroMock).orElse(libroMock));
		when(libroDao.findById(1)).thenReturn(libroMockOptional);
	}
	
	@Test
	public void obtenerLibroId() {
		setup2();
		libroService.libroDao = libroDao;
		Libro respuesta = libroDao.findById(1).orElse(null);
		Autor autor = new Autor();
		Libro libro = new Libro();
		autor.setId(1);
		libro.setAutor(autor);
		libro.setId(1);
		assertEquals(libro.getId(), respuesta.getId());
	}
	
	
	
	void setupObtenerLibrosIdAutorContenido() {
		Autor autor = new Autor();
		autor.setNombre("Pepe");
		autor.setApellidos("garcia");
		autor.setAñosExperiencia(3);
		autor.setEdad(40);
		autor.setId(1);
		Libro libroMock = new Libro();
		libroMock.setId(1);
		libroMock.setTitulo("Titulo");
		libroMock.setPrecio(25000);
		libroMock.setTema("astro");
		libroMock.setAutor(autor);
		Libro libroMock2 = new Libro();
		libroMock2.setId(2);
		libroMock2.setTitulo("Titulo ciencia");
		libroMock2.setPrecio(40000);
		libroMock2.setTema("Ciencia");
		libroMock2.setAutor(autor);
		List<Libro> librosMock = new ArrayList<>();
		librosMock.add(libroMock);
		librosMock.add(libroMock2);
		when(libroDao.findByIdAutor(1)).thenReturn(librosMock);
	}
	
	@Test
	void testObtenerLibrosIdAutorCorrecto() {
		setupObtenerLibrosIdAutorContenido();
		libroService.libroDao = libroDao;
		ResponseEntity<Object> respuesta = libroService.obtenerLibrosIdAutor(1);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
	}
	
	
	void setupObtenerLibrosIdAutor() {
		List<Libro> librosMock = new ArrayList<>();
		when(libroDao.findByIdAutor(1)).thenReturn(librosMock);
	}
	
	@Test
	void obtenerLibrosIdAutorCatch() {
		setupObtenerLibrosIdAutor();
		libroService.libroDao = libroDao;
		ResponseEntity<Object> respuesta = libroService.obtenerLibrosIdAutor(1);
		assertEquals(HttpStatus.BAD_REQUEST, respuesta.getStatusCode());
	}
	
	
	void SetupSave(){
		Autor autor = new Autor();
		autor.setId(1);
		Libro libro = new Libro();
		libro.setId(2);
		libro.setTitulo("Titulo");
		libro.setPrecio(25000);
		libro.setTema("astro");
		libro.setAutor(autor);
		libroService.libroDao = libroDao;
		when(libroDao.save(Mockito.any(Libro.class))).thenReturn(libro);
	}
	
	@Test
	void GuardarLibro(){
		SetupSave();
		Autor autor = new Autor();
		autor.setId(1);
		Libro libro = new Libro();
		libro.setId(1);
		libro.setTitulo("Titulo");
		libro.setPrecio(25000);
		libro.setTema("astro");
		libro.setAutor(autor);
		Libro respuesta = libroService.agregarLibro(libro);
		System.out.println(respuesta);
		assertEquals(libro.getAutor().getId(), respuesta.getAutor().getId());
	}
	
}
