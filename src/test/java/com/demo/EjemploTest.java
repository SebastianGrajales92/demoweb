package com.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.aspectj.lang.annotation.After;
import org.junit.Before;
import org.junit.Test;

public class EjemploTest {

	private int a;
	private int b;
	
	@Before
	public void setup() {
		a = 2;
		b = 3;
	}
	
	@Test
	public void probarSumar() {
		assertEquals(5, a+b);
	}
	
	@Test
	public void probarResta() {
		assertEquals(1, b-a);
	}
}
